package com.online.library.util.common;

public class SimpleResponse {

	private String message;
	private Object payload;
	
	public SimpleResponse(String message, Object payload) {
		this.message = message;
		this.payload = payload;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}
}
