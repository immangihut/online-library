package com.online.library.book.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.online.library.book.dto.BookDto;
import com.online.library.book.entity.BookEntity;
import com.online.library.book.repository.BookRepository;
import com.online.library.util.common.SimpleResponse;

@Service
public class BookService {
	
    private static final Logger log = LoggerFactory.getLogger(BookService.class);

	@Autowired
	private BookRepository bookRepository;
	
	@Transactional(readOnly = false)
	public SimpleResponse addBook(BookDto dto) {
        BookEntity bookEntity = new BookEntity(dto);
        BookEntity savedEntity = bookRepository.save(bookEntity);
        
        return new SimpleResponse("Book added successfully", savedEntity.parseDto());
    }
	
	@Transactional(readOnly = true)
    public SimpleResponse getBookById(BookDto dto) {
		UUID id = UUID.fromString(dto.getBookId());
        Optional<BookEntity> bookEntity = bookRepository.findById(id);
        
        if (bookEntity.isPresent()) {
            return new SimpleResponse("Book retrieved successfully", bookEntity.get().parseDto());
        }
        
        return new SimpleResponse("Book not found", null);
    }
	
	@Transactional(readOnly = true)
    public SimpleResponse getAllBooks() {
        List<BookEntity> bookEntities = bookRepository.findAll();
        List<BookDto> bookDTOs = bookEntities.stream()
                                             .map(BookEntity::parseDto)
                                             .collect(Collectors.toList());
        return new SimpleResponse("Books retrieved successfully", bookDTOs);
    }
	
	@Transactional(readOnly = false)
	public SimpleResponse editBook(BookDto dto) {
		UUID id = UUID.fromString(dto.getBookId());
        BookEntity book = bookRepository.findByBookId(id);
        
        if (book != null) {
            if (dto.getAuthor() != null) {
                book.setAuthor(dto.getAuthor());
            }
            if (dto.getCategory() != null) {
                book.setCategory(dto.getCategory());
            }
            if (dto.getDescription() != null) {
                book.setDescription(dto.getDescription());
            }
            if (dto.getPublishedDate() != null) {
                book.setPublishedDate(dto.getPublishedDate());
            }
            if (dto.getPublisher() != null) {
                book.setPublisher(dto.getPublisher());
            }
            if (dto.getTitle() != null) {
                book.setTitle(dto.getTitle());
            }
        	
        	bookRepository.save(book);
        	return new SimpleResponse("Book updated successfully", book);
        }
        
        return new SimpleResponse("Book not found", null);
	}
	
	@Transactional(readOnly = false)
    public SimpleResponse deleteBook(BookDto dto) {
    	UUID id = UUID.fromString(dto.getBookId());
    	
        if (bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
            return new SimpleResponse("Book deleted successfully", null);
        }
        
        return new SimpleResponse("Book not found", null);
    }
	
	@Transactional(readOnly = true)
	public SimpleResponse searchBooksByTitleAndAuthor(BookDto dto) {
		
		if (dto.getTitle().isEmpty() || dto.getAuthor().isEmpty()) {
			throw new IllegalArgumentException("Title and author must not be null");
		}
		
		String title = dto.getTitle();
		String author = dto.getAuthor();
		
		List<BookEntity> books = bookRepository.findByTitleAndAuthor(title, author);
		
		return new SimpleResponse("Books found", books);
	}
	
	@Transactional(readOnly = true)
	public SimpleResponse getBooksByPublishedDateRange(Date startDate, Date endDate) {
		log.info("Searching for books from {} to {}", startDate, endDate);
		
		if (startDate == null || endDate == null) {
			throw new IllegalArgumentException("Start date and end date must not be null");
		}
		
		List<BookEntity> books = bookRepository.findByPublishedDateRange(startDate, endDate);
		
		log.info("Found {} books", books.size());
		
		return new SimpleResponse("Books found", books);
	}
	
}
