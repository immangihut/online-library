package com.online.library.book.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.online.library.book.dto.BookDto;
import com.online.library.book.service.BookService;
import com.online.library.util.common.SimpleResponse;

@RestController
@RequestMapping("api/book")
public class BookController {

    private static Logger log = LoggerFactory.getLogger(BookService.class);
    
	private BookService bookService;
	
	public BookController(BookService bookService) {
        this.bookService = bookService;
    }
	
	@PostMapping("/add")
    public SimpleResponse addBook(@RequestBody BookDto bookDTO) throws Exception {
		try {
			return bookService.addBook(bookDTO);
		} catch(Exception e) {
			log.error("ADD BOOK ERROR", e);
			throw new RuntimeException(e);
		}
    }
	
	@PostMapping("/detail")
    public SimpleResponse getBookById(@RequestBody BookDto dto) throws Exception {
		try {
			return bookService.getBookById(dto);
		} catch (Exception e) {
			log.error("GET BOOK ERROR", e);
			throw new RuntimeException(e);
		}
    }
	
	@GetMapping("/list")
    public SimpleResponse getAllBooks() throws Exception {
		try {
			return bookService.getAllBooks();
		} catch (Exception e) {
			log.error("GET LIST OF BOOKS ERROR", e);
			throw new RuntimeException(e);
		}
    }
	
	@PostMapping("/edit")
	public SimpleResponse editBoook(@RequestBody BookDto dto) throws Exception {
		try {
			return bookService.editBook(dto);
		} catch (Exception e) {
			log.error("EDIT BOOK ERROR", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/delete")
	public SimpleResponse deleteBoook(@RequestBody BookDto dto) throws Exception {
		try {
			return bookService.deleteBook(dto);
		} catch (Exception e) {
			log.error("DELETE BOOK ERROR", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/search")
	public SimpleResponse searchBookByTitleAndAuthor(@RequestBody BookDto dto) throws Exception {
		try {
			return bookService.searchBooksByTitleAndAuthor(dto);
		} catch(Exception e) {
			log.error("SEARCH BOOK ERROR");
			throw new RuntimeException(e);
		}
	}
	
	@GetMapping("/search/date-range")
	public SimpleResponse getBooksByPublishedDateRange(
	        @RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
	        @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws Exception {
	    try {
	        return bookService.getBooksByPublishedDateRange(startDate, endDate);
	    } catch(Exception e) {
	        log.error("SEARCH BOOK ERROR", e);
	        throw new RuntimeException(e);
	    }
	}
}
