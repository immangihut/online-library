package com.online.library.book.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.online.library.book.dto.BookDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "books")
@Data
@EqualsAndHashCode(callSuper = false)
public class BookEntity {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "book_id", updatable = false, nullable = false)
	private UUID bookId;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "author")
	private String author;
	
	@Temporal(TemporalType.DATE)
    @Column(name = "published_date")
	private Date publishedDate;
	
	@Column(name = "category")
	private String category;
	
	@Column(name = "publisher")
	private String publisher;
	
	@Column(name = "description", length = 2000)
	private String description;
	
	public BookEntity() {
		
	}
	
	public BookEntity(BookDto dto) {
		if (dto.getBookId() != null) {
			this.bookId = UUID.fromString(dto.getBookId());
		}
        this.title = dto.getTitle();
        this.author = dto.getAuthor();
        this.publishedDate = dto.getPublishedDate();
        this.category = dto.getCategory();
        this.publisher = dto.getPublisher();
        this.description = dto.getDescription();
    }
	
	public BookDto parseDto() {
		BookDto dto = new BookDto();
		dto.setBookId(this.bookId.toString());
		dto.setTitle(this.title);
		dto.setAuthor(this.author);
		dto.setPublishedDate(this.publishedDate);
		dto.setCategory(this.category);
		dto.setPublisher(this.publisher);
		dto.setDescription(this.description);
		return dto;
	}

}
