package com.online.library.book.repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.online.library.book.entity.BookEntity;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, UUID> {

	BookEntity findByBookId(UUID bookId);
	
	@Query(value = "SELECT * FROM books WHERE title LIKE %:title% AND author LIKE %:author%", nativeQuery = true)
	List<BookEntity> findByTitleAndAuthor(@Param("title") String title, @Param("author") String author);
	
	@Query(value = "SELECT * FROM books WHERE published_date BETWEEN :startDate AND :endDate", nativeQuery = true)
    List<BookEntity> findByPublishedDateRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
