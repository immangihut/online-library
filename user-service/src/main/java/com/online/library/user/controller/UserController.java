package com.online.library.user.controller;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.online.library.user.dto.UserDto;
import com.online.library.user.service.UserService;
import com.online.library.util.common.SimpleResponse;

@RestController
@RequestMapping("api/user")
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserService.class);
	
	private UserService userService;
	
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	@PostMapping("/register")
	public SimpleResponse registerNewUser(@RequestBody UserDto dto) throws Exception {
		try {
			return userService.registerNewUser(dto);
		} catch(Exception e) {
			log.error("REGISTER NEW USER ERROR", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/detail")
	public SimpleResponse getUserDetail(@RequestBody UserDto dto) throws Exception {
		try {
			return userService.getDetailUser(dto);
		} catch(Exception e) {
			log.error("GET DETAIL USER ERROR", e);
			throw new RuntimeException(e);
		}
	}
	
	@PostMapping("/add-favorite")
	public SimpleResponse addFavoriteBook(
	        @RequestParam UUID userId, 
	        @RequestParam UUID bookId) {
	    try {
	    	return userService.addFavoriteBook(userId, bookId);
	    } catch (Exception e) {
	        log.error("Error adding favorite book", e);
	        throw new RuntimeException(e);
	    }
	}

	
}
