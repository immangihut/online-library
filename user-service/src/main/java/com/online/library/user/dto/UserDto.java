package com.online.library.user.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String userId;
	private String userName;
	private String password;
	private String email;
	
}
