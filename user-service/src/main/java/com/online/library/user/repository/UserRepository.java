package com.online.library.user.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.online.library.user.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, UUID>{

	Optional<UserEntity> findByUserName(String username);
}
