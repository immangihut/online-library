package com.online.library.user.service;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.online.library.book.entity.BookEntity;
import com.online.library.book.repository.BookRepository;
import com.online.library.user.dto.UserDto;
import com.online.library.user.entity.UserEntity;
import com.online.library.user.repository.UserRepository;
import com.online.library.util.common.SimpleResponse;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Transactional(readOnly = false)
	public SimpleResponse registerNewUser(UserDto dto) {
		if (userRepository.findByUserName(dto.getUserName()).isPresent()) {
			throw new RuntimeException("Username already exists.");
		}
		
		UserEntity user = new UserEntity();
		user.setUserName(dto.getUserName());
		user.setPassword(passwordEncoder.encode(dto.getPassword()));
		user.setEmail(dto.getEmail());
		
		userRepository.save(user);
		return new SimpleResponse("User registered successfully", user);
	}
	
	@Transactional(readOnly = true)
	public SimpleResponse getDetailUser(UserDto dto) {
		UUID id = UUID.fromString(dto.getUserId());
		Optional<UserEntity> user = userRepository.findById(id);
		if (user.isPresent()) {
			return new SimpleResponse("User retrieved successfully", user.get().parseDto());
		}
		return new SimpleResponse("User not found", null);
	}
	
	@Transactional(readOnly = false)
	public SimpleResponse addFavoriteBook(UUID userId, UUID bookId) {
		
		UserEntity user = userRepository.findById(userId)
				.orElseThrow(() -> new RuntimeException("User not found"));
		
		BookEntity book = bookRepository.findById(bookId)
                .orElseThrow(() -> new RuntimeException("Book not found"));
		
		user.getFavoriteBooks().add(book);
		userRepository.save(user);
		
		return new SimpleResponse("Book added to favorites", user.getFavoriteBooks());
		
	}
}
