package com.online.library.user.entity;

import java.util.HashSet;

import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.GenericGenerator;

import com.online.library.book.entity.BookEntity;
import com.online.library.user.dto.UserDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "users")
@Data
@EqualsAndHashCode(callSuper = false)
public class UserEntity {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "user_id", updatable = false, nullable = false)
	private UUID userId;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "email")
	private String email;
	
    @ManyToMany
    @JoinTable(
        name = "user_favorite_books",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "book_id")
    )
    private Set<BookEntity> favoriteBooks = new HashSet<>();
	
	public UserEntity() {
		
	}
	
	public UserEntity(UserDto dto) {
		if (dto.getUserId() != null) {
			this.userId = UUID.fromString(dto.getUserId());
		}
		this.userName = dto.getUserName();
		this.password = dto.getPassword();
		this.email = dto.getEmail();
	}
	
	public UserDto parseDto() {
		UserDto dto = new UserDto();
		dto.setUserId(this.userId.toString());
		dto.setUserName(this.userName);
		dto.setPassword(this.password);
		dto.setEmail(this.email);
		return dto;
	}
	
}
